# Author Kim Nilsson, Otto Nordgren
# Stockholm 2012
#
# API Specifikation
# https://docs.google.com/a/armada.nu/document/d/1KFNSIShO2MvY3nfYzHb2a0GURfOk9rRAq3lake6eGAM/edit#

module API
	class Placement < ::Sinatra::Base
		before do
			content_type 'application/json'
		end

		# GET /
		get '/' do
			'Bordeplacering API, se dokumentationsfil pa Google docs for anrops specifikationer!'
		end

		# POST /do_placement
		# Indata: En hash med tillängliga bord samt personer som ska placeras.
		# Utdata: En hash med personer.
		# 
		# För mer utförlig dokumentation se Google docs, 'API Dokumentation'
		post '/do_placement' do
			response.headers["Access-Control-Allow-Origin"] = "*"
			response.headers["Access-Control-Allow-Methods"] = "POST"

     callback = params.delete('callback') # jsonp
     json = {'your' => 'data'}.to_json

			if params['tables'].nil? or params['persons'].nil?
				halt '400', 'Argument error. You must provide both tables and persons.'
			end

			tables = []
			begin
				tables_hash = JSON.parse(params['tables'])
				tables_hash.each do |table_hash|
					tables << Table.new_from_hash(table_hash)
				end
			rescue
				halt '500', 'Tables data incorrectly formatted.'
			end

			begin 
				persons = []
				persons_hash = JSON.parse(params['persons'])
				persons_hash.each do |person|
					persons << Person.new_from_hash(person)
				end
			rescue
				halt '500', 'Persons data incorrectly formatted.'
			end

			persons.each do |person1|
				persons.each do |person2|
					dist, report = person1.distance(person2)		
					if development?
						puts "Distance between #{person1.name} and #{person2.name} is #{dist}.\n"
						puts report unless report.nil? or report == ""
					end
				end
			end


			filename = "output/weights.tsp"

			if filename.include? 'tsp'
				report_matrix = Bordsplacering.write_tsp_file(persons, filename)
			else
				report_matrix = Bordsplacering.write_distance_matrix(persons, filename)
			end

			# Store report_matrix
			File.open('output/report', "w") do |f|
				report_matrix.each do |row|
					row.each do |value, key|
						f.puts value.to_yaml
					end
					f.puts ""
					f.puts "------------------------------------------"
					f.puts "------------------------------------------"
					f.puts ""
				end
			end

			data, fitness = Bordsplacering.place filename

			puts "This is data:"
			puts data.inspect

			# Place people at tables
			tables.each do |table|
				ids_at_table = data.slice!(0, table.size)	
				ids_at_table.each_with_index do |id, seat_nr|
					puts id
					persons[id].placement['table_id'] = table.id
					persons[id].placement['seat'] = seat_nr
				end
			end

			# Add report
=begin
			puts "Adding reports"
			persons.each do |person|
				left_neighbour = right_neighbour = nil

				persons.each do |person2|
					if person2.placement['table_id'] == person.placement['table_id'] 
						if person2.placement['seat'] == person.placement['seat']+1
							right_neighbour = person2
						end
						if person2.placement['seat'] == person.placement['seat']-1
							left_neighbour = person2
						end
					end
				end

				person.report = {}
				if left_neighbour
					person.report['left'] = left_neighbour.name + ": " + report_matrix[person.id][left_neighbour.id]
				end

				if right_neighbour
					person.report['right'] = right_neighbour.name + ": " +  report_matrix[person.id][right_neighbour.id]
				end
			end
=end

			if development?
				{data: data, fitness: fitness, tables: tables, persons: persons}.to_json
			else
				{persons: persons, tables: tables, fitness: fitness}.to_json
			end
		end

		post '/testwithtables' do
			tables = []
			begin
				tables_hash = JSON.parse(params['tables'])
				tables_hash.each do |table_hash|
					tables << Table.new_from_hash(table_hash)
				end
			rescue
				halt '500', 'Tables data incorrectly formatted.'
			end

			persons = PlacementTest.spawn_persons 150 

			filename = "output/weights.tsp"

			if filename.include? 'tsp'
				report_matrix = Bordsplacering.write_tsp_file(persons, filename)
			else
				report_matrix = Bordsplacering.write_distance_matrix(persons, filename)
			end

			data, fitness = Bordsplacering.place filename

			seats = %w[a b c d e f g h i j k l m n o p r q r s t u v w x y z]
			tables.each do |table|
				ids_at_table = data.slice!(0, table.size)	
				ids_at_table.each_with_index do |id, seat_nr|
					persons[id].placement['table_id'] = table.id
					persons[id].placement['seat'] = seats[seat_nr]
				end
			end

			# Add report
			puts "Adding reports"
			persons.each do |person|
				left_neighbour = right_neighbour = nil

				persons.each do |person2|
					if person2.placement['table_id'] == person.placement['table_id'] 
						if person2.placement['seat'] == person.placement['seat']+1
							right_neighbour = person2
						end
						if person2.placement['seat'] == person.placement['seat']-1
							left_neighbour = person2
						end
					end
				end

				person.report = {}
				if left_neighbour
					person.report['left'] = left_neighbour.name.to_s + ": " + report_matrix[person.id][left_neighbour.id]
				end

				if right_neighbour
					person.report['right'] = right_neighbour.name.to_s + ": " +  report_matrix[person.id][right_neighbour.id]
				end
			end			

			{data: data, fitness: fitness, tables: tables, persons: persons}.to_json
		end

		post '/test' do
			persons = PlacementTest.spawn_persons 1000

			#filename = "output/distance-matrix-#{Time.now.to_s}.csv"
			filename = "output/weights.tsp"

			if filename.include? 'tsp'
				report_matrix = Bordsplacering.write_tsp_file(persons, filename)
			else
				report_matrix = Bordsplacering.write_distance_matrix(persons, filename)
			end

			start = Time.now
			data, fitness = Bordsplacering.place filename

			puts "Duration: #{Time.now - start}"
			{fitness: fitness, data: data}.to_json
		end

		post '/testlastrun' do
			data, fitness = Bordsplacering.place 'test-data.csv'
			{fitness: fitness, data: data}.to_json
		end
	end
end