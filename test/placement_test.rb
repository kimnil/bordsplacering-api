module PlacementTest

	COMPANYS = %w{ibm scania}
	STUDENTS = %W{datateknik maskinteknik flyg kemi fysik cl}

	# Spawn some dummy persons
	def self.spawn_persons num
		persons = []
		num.times do |int|
			seed = rand num * 10000
			seed2 = rand num * 10000
			p = Person.new
			p.id = int
			p.name = seed
			p.gender = (seed % 2 == 0 ? 'male' : 'female')
			if seed % 3 == 0
				p.type = {'company' => COMPANYS[seed2 % COMPANYS.size]}
				p.interests = [{'type' => {'student' => STUDENTS[seed % STUDENTS.size]}, 'weight' => 500}]
			else
				p.type = {'student' => STUDENTS[seed2 % STUDENTS.size]}
				p.interests = [{'type' => {'company' => COMPANYS[seed % COMPANYS.size]}, 'weight' => 500}]
			end
			p.placement = {'table_id' => -1, 'seat' => -1}
			puts "spawned #{p.name}, #{p.gender}" 
			persons << p
		end
		persons
	end
end