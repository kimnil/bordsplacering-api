require "rubygems"
require "bundler/setup"
require 'sinatra'
require 'sinatra/reloader' if development?
require 'json'
require 'csv'
require 'sinatra/cross_origin'
require 'yaml'

configure do
end


root = ::File.dirname(__FILE__)

["placement.rb"].each { |f| require ::File.join( root, f )}

Dir["./lib/*.rb"].each { |f| require f }
Dir["./models/*.rb"].each { |f| require f }
Dir["./test/*.rb"].each { |f| require f }


run Rack::URLMap.new(
  '/'           => API::Placement
)
