# This module contains the seating logic.
# authors: Kim Nilsson, Otto Nordgren

module Bordsplacering
	require 'rubygems'
	require 'ai4r'

  def Bordsplacering.write_distance_matrix(persons, filename)
		report_matrix = Hash.new

		puts "Writing distance matrix to #{filename}\n"
		CSV.open(filename, "w") do |csv_file|
			persons.each do |person1|
				row = []
				persons.each do |person2|
					dist, report = person1.distance(person2)		
					row << dist

					(report_matrix[person1.id] ||= {})[person2.id] = report

					if development?
						puts "Distance between #{person1.name} and #{person2.name} is #{dist}.\n"
						puts report unless report.nil? or report == ""
					end
				end
				csv_file << row 
			end
		end

		report_matrix
	end

	def Bordsplacering.write_tsp_file(persons, filename)
		report_matrix = Hash.new


		puts "Writing TSP to #{filename}\n"

		File.open(filename, "w") do |file|

			file.puts "NAME : Bankettplacering"
			file.puts "COMMENT : Armada 2012s placering"
			file.puts "TYPE : TSP"
			file.puts "DIMENSION : " + persons.length.to_s
			file.puts "EDGE_WEIGHT_TYPE : EXPLICIT"
			file.puts "EDGE_WEIGHT_FORMAT : FULL_MATRIX"
			file.puts "NODE_COORD_TYPE : NO_COORDS"
			file.puts "DISPLAY_DATA_TYPE : NO_DISPLAY"
			file.puts "EDGE_WEIGHT_SECTION"

			persons.each do |person1|
				row = []
				persons.each do |person2|
					dist, report = person1.distance(person2)		
					row << dist

					(report_matrix[person1.id] ||= {})[person2.id] = report

					if development?
						puts "Distance between #{person1.name} and #{person2.name} is #{dist}.\n"
						puts report unless report.nil? or report == ""
					end
				end
				file.write row.join(' ')  + "\n"
			end

			file.puts "\nEOF"
		end

		report_matrix
	end


	# How this will communicate (that is file input or arguments etc) is 
	# not yet defined. Hence the ARGV code in this method.
	def self.place(input_file)
    if input_file.include? '.csv'
			puts "CSV File. Running ai4r GeneticAlgorithm algorithm."
			# Data set is the distance matrix between nodes.
			data_set = []

			begin
				puts "Reading #{input_file}"
				CSV.foreach input_file do |row|
					unless row[0][0] == '#'
						data_set << row.map(&:to_i)
					end	
				end
				puts "Done reading input."
				#puts data_set.inspect
			rescue Exception => e
				puts "Couldn't open file."
				puts e
				exit 1
			end

			Ai4r::GeneticAlgorithm::Chromosome.set_cost_matrix(data_set)

			3.times do
			  c = Ai4r::GeneticAlgorithm::Chromosome.seed
			  puts "COST #{-1 * c.fitness}"
			end

			# Första parametern är antalet kromosomer som initialt genereras.
			# Andra parametern är antalet mutationer innan svar ges.
			search = Ai4r::GeneticAlgorithm::GeneticSearch.new(800,130)
			puts "Solving TSP"
			result = search.run
			puts "Done"

			puts "Result cost: #{result.fitness}"
			puts "Result nodes: #{result.data.inspect}"
			puts result.data
			return result.data, result.fitness
    else
      puts "Running LKH"
      #exec "./LKH", "test.par"
      `lib/./LKH lkh_problem.par`

      puts "----------------"
      puts "Done. Starting to read output:"

      f = File.new("output/lkh_output", "r")

      sleep 2

      found_tour = false
      tour = []
      while (line = f.gets)
        puts line

        if line.start_with? '-1'
          puts "end of tour"
          break
        end

        if found_tour
          tour << (line.to_i - 1)
        end

        if line.include? 'SECTION'
          puts "section line"
          found_tour = true
        end
      end

      f.close	

      puts "End of file."

      return tour, "?"
    end
  end
end
