$(function() {

  // Which API to use.
  //var api_url = "http://localhost:9292/do_placement";
  var api_url = "http://bordsplacering.armada.nu:9292/do_placement";
  var api_url = "http://localhost:9090/do_placement";

  $('#export-data').click(function() {
    var result = [],
        person;


    _.each($('.table'), function(table) { 
        var table_name = $(table).data('table-label');
        var table_nr = $(table).data('table-id');

      _.each($('.person', table), function(person, seat_nr) {


        var person_id = $(person).data('person-id');
        var person_type = $(person).data('person-type');
        var person_name = $(person).text();

        person = {
          id: person_id,
          table_id: table_nr,
          seat: seat_nr + 1 // +1 so its not 0-indexed
        }

        result.push(person)
      })
    });

    console.log("Result:");
    console.log(result);
    console.log(JSON.stringify(result));

    $.modal("<div class=\"output\">" + JSON.stringify(result) + "</div>");
    //copyToClipboard(JSON.stringify(result));
  });

  /*
    Neat little function that prompts the user to Copy-C some text.
  */
  function copyToClipboard (text) {
    window.prompt ("Copy to clipboard: Ctrl+C, Enter", text);
  } 

  $('#get-data').click(function() {
    $.ajax(api_url, {
      type: 'POST',
      data: { 
        tables: $('#input-tables').val() || '[{"id": 1, "label": 1, "size": 10}, {"id": 2, "label": 2, "size": 10}]', 
        persons: $('#input-persons').val() || '[ {"id": 1, "name": "Kim Nilsson", "gender": "unknown", "type": {"student": 2712}, "interests": [{"weight": 200, "type": {"company": "cisco"}}], "placement": {"table_id": -1, "seat": -1}},  {"id": 2, "name": "Otto Nordgren", "gender": "male", "type": {"student": "datateknik"}, "interests": [], "placement": {"table_id": -1, "seat": -1}},  {"id": 3, "name": "Staffan Gabrielsson", "gender": "male", "type": {"student": "datateknik"}, "interests": [{"weight": 0, "type": {"company": "apple"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 4, "name": "Martin Löwhagen", "gender": "male", "type": {"student": "maskinteknik"}, "interests": [{"weight": 0, "type": {"company": "scania"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 5, "name": "Sebastian Bremberg", "gender": "male", "type": {"student": "teknisk_fysik"}, "interests": [{"weight": 0, "type": {"company": "cern"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 6, "name": "Josefin Reinelöv", "gender": "female", "type": {"student": "industriell_ekonomi"}, "interests": [{"weight": 0, "type": {"company": "bcg"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 7, "name": "Martin Koskela", "gender": "male", "type": {"student": "maskinteknik"}, "interests": [{"weight": 0, "type": {"company": "scania"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 8, "name": "Karol Gonzales", "gender": "female", "type": {"student": "kemiteknik"}, "interests": [{"weight": 0, "type": {"company": "skc"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 9, "name": "Martina Bergman", "gender": "female", "type": {"student": "design_produkt"}, "interests": [{"weight": 0, "type": {"company": "åf"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 10, "name": "Hanna Petterson", "gender": "female", "type": {"student": "mediateknik"}, "interests": [{"weight": 0, "type": {"company": "netlight"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 11, "name": "Olivia Lindgren", "gender": "female", "type": {"student": "maskinteknik"}, "interests": [{"weight": 0, "type": {"company": "volvo"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 12, "name": "Emma Martensson", "gender": "female", "type": {"student": "maskinteknik"}, "interests": [{"weight": 0, "type": {"company": "saab"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 13, "name": "Kim Nilsson F", "gender": "male", "type": {"company": "cisco"}, "interests": [{"weight": 0, "type": {"student": "datateknik"}}], "placement": {"table_id": -1, "seat": -1}},  {"id": 14, "name": "Otto Nordgren F", "gender": "male", "type": {"company": "ibm"}, "interests": [{"weight": 0, "type": {"student": "datateknik"}}], "placement": {"table_id": -1, "seat": -1}},  {"id": 15, "name": "Staffan Gabrielsson F", "gender": "male", "type": {"company": "apple"}, "interests": [{"weight": 0, "type": {"student": "datateknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 16, "name": "Martin Löwhagen F", "gender": "male", "type": {"company": "scania"}, "interests": [{"weight": 0, "type": {"student": "maskinteknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 17, "name": "Sebastian Bremberg F", "gender": "male", "type": {"company": "cern"}, "interests": [{"weight": 0, "type": {"student": "teknisk_fysik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 18, "name": "Josefin Reinelöv F", "gender": "female", "type": {"company": "bcg"}, "interests": [{"weight": 0, "type": {"student": "industriell_ekonomi"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 19, "name": "Martin Koskela F", "gender": "male", "type": {"company": "scania"}, "interests": [{"weight": 0, "type": {"student": "maskinteknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 20, "name": "Karol Gonzales F", "gender": "female", "type": {"company": "skc"}, "interests": [{"weight": 0, "type": {"student": "kemiteknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 21, "name": "Martina Bergman F", "gender": "female", "type": {"company": "åf"}, "interests": [{"weight": 0, "type": {"student": "design_produkt"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 22, "name": "Hanna Petterson F", "gender": "female", "type": {"company": "netlight"}, "interests": [{"weight": 0, "type": {"student": "mediateknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 23, "name": "Olivia Lindgren F", "gender": "female", "type": {"company": "volvo"}, "interests": [{"weight": 0, "type": {"student": "maskinteknik"}}], "placement": {"table_id": -1, "seat": -1}}, {"id": 24, "name": "Emma Martensson F", "gender": "female", "type": {"company": "saab"}, "interests": [{"weight": 0, "type": {"student": "maskinteknik"}}], "placement": {"table_id": -1, "seat": -1}} ]'
      },
      beforeSend: $.blockUI({
        message: '<h2>Fetching from API</h2>'            
      }),

      success: function(data) {
        populateTables(data);
        updateSeats();
        drawTables();
        makeMoveable();
        $.unblockUI();
      },
      error: function(error) {
        $.blockUI({
          message: error.status  + ' Error'
        });;
        setTimeout($.unblockUI(), 2500);
        console.log("eeey errro");
      },
      async: true
    });
    return false;
  });

  function drawTables()
  {
    var radius = 85;

    $(".seats li").each(function(index) {
      var alpha = Math.PI * 2 / $(this).parent().children().length;
      var theta = (alpha * index) - Math.PI / 2;
      var x = Math.cos(theta) * radius;
      var y = Math.sin(theta) * radius;

      $(this).css({
        'position': 'absolute',
        'margin-left': x,
        'margin-top': y
      });
    });
  }

  function updateSeats()
  {
    $('.seats li').remove();
    $('.table').each(function() {
      persons = $(this).find('.persons li');
      seats = $(this).find('.seats');
      persons.each(function() {
        var gender = $(this).find('.person').attr('data-person-gender');
        var type = $(this).find('.person').attr('data-person-type');
        person = $('<li>').addClass(gender).addClass(type).attr('data-person-id', $(this).attr('data-person-id')).attr('data-person-name', $(this).text());
        seats.append(person);
      });
    });
  }

  function populateTables(response) {
    $('.tables').empty();
    $(response.tables).each(function() {
      table = $('<div>').addClass('table float-left').attr('data-table-id', this.id).attr('data-table-label', this.label);
      $('.tables').append(table);

      list = $('<ul>').addClass('persons float-left');
      seats = $('<ul>').addClass('seats float-left');
      table.append(list).append(seats);
      for(index = 1; index <= this.size; index++) {
        list_item = $('<li>').addClass('person-list');
        list.append(list_item);
      }
    });

    $(response.persons).each(function() {
      var type = this.type.company ? 'company' : 'student';

      seat = $('[data-table-id="' + this.placement.table_id + '"] ul li')[this.placement.seat];
      person_holder = $('<div>').addClass('person-holder');
      person = $('<div>').addClass('person ' + this.gender + ' ' + type)
        .attr('data-person-id', this.id)
        .attr('data-person-gender', this.gender)
        .attr('data-person-type', type);

      var name = $('<span>').addClass('name').html(this.name.substring(0, 17));

      $(person).append(name);

      if(type === "student" && this.interests.length > 0) {
        var id = $('<span>').addClass('id').html(this.type.student);
        $(person).append(id);
      }

      if(type === "company") {
        var id = $('<span>').addClass('id').html(this.type.company);
        $(person).append(id);
      }

      var sign = $('<div>').addClass('sign').html("<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'><svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='25px' height='25px' xml:space='preserve'> <path id='book-17-icon' d='M441.418,158.455v195.167l-12.445,2V158.455V118.97c-66.856,2.04-130.146,12.788-173,39.466 c-42.856-26.678-106.144-37.426-173-39.466v39.485v197.167l-12.445-2V158.455h-20.5v219.001h155.45 c25.376,0,28.161,15.574,50.495,15.574c22.376,0,25.071-15.574,50.496-15.574h155.504V158.455H441.418z M238.982,352.892 c-33.402-13.253-73.892-21.78-123.029-25.474V154.9c40.354,3.386,88.635,11.587,123.029,32.984V352.892z M395.992,327.418 c-49.138,3.693-89.628,12.221-123.029,25.474V187.885c34.394-21.397,82.675-29.599,123.029-32.984V327.418z'/> </svg>");

      person.append(sign);

      $(seat).append(person_holder);
      person_holder.append(person);
      person_holder.parent().attr('data-person-id', this.id).addClass(this.gender).addClass(type);
    });
  }

  function makeMoveable() {
    // Options for the swappable inner object
    src = null;
    options = {
      snap: ".person-list",
      snapMode: "inner",
      appendTo: "body",
      helper: "clone",
      start: function() {
        src = $(this).parent();
        $(this).hide();
      },
      revert: function(person) {
        if(person === false) {
          $(this).show();
          $('.swapholder').remove();
          updateGenders();
          return true;
        }
        else {
          return false;
        }
      }
    };
    $(".person").draggable(options);

    // Make each big li sortable
    $(".persons").sortable({
      axis: 'y',
      update: function(event, ui) {
        updateSeats();
        drawTables();
      },
      placeholder: 'placeholder'
    });


    // Allow the inner div to be dropped and swapped with draggable
    $(".person-holder").droppable({
      over: function(event, ui) {
        over = $('.person', this);
        $('.swapholder').remove();     
        src.closest('li').removeClass('female male').addClass(over.attr('data-person-gender'));
        if(over.attr('data-person-id') != ui.helper.attr('data-person-id')) {
          src.append('<li class="swapholder">' + over.html() + '</li>');
        }
      },
      drop: function(event, ui) {
        var dragged = ui.draggable.clone().removeClass().addClass('person').css({'top': '', 'left': ''}).draggable(options);
        var dropped =  $('.person', this).clone().removeClass().addClass('person').css({'top': '', 'left': ''}).draggable(options);
        src.empty().append(
          $(dropped).show()
        );
        $(this).empty().append(
          $(dragged).show()
        );

        updateGenders();

        ui.helper.remove();
        $('.swapcontainer').removeClass('swapcontainer');

        updateSeats();
        drawTables();
      },
      accept: ".person"
    });


    $(".persons").on({
      mouseenter: function() {
        $(this).addClass('hover');
        $('.seats [data-person-id="' + $(this).attr('data-person-id') + '"]').addClass('hover');
      },
      mouseleave: function() {
        $(this).removeClass('hover');
        $('.seats [data-person-id="' + $(this).attr('data-person-id') + '"]').removeClass('hover');
      }
    }, "li");

    updateSeats();
    drawTables();
  }

  function updateGenders() {
    $(".persons li").each(function() {
      child = $(this).children().children();
      gender = $(this).children().children().attr('data-person-gender');
      type = $(this).children().children().attr('data-person-type');
      $(this).removeClass('female male company student');
      child.removeClass('female male company student');
      child.addClass(gender).addClass(type);
      $(this).addClass(gender).addClass(type);
      $(this).attr('data-person-id', $(this).children().children().attr('data-person-id'));
    });
  }

});
