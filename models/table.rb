# Author Kim Nilsson, Otto Nordgren
# Stockholm 2012
class Table

	attr_accessor :id
	attr_accessor :label
	attr_accessor :size

	def self.new_from_hash table_hash
		unless table_hash.instance_of? Hash
			halt 500
		end

		table = Table.new
		table.id = table_hash['id']
		table.label = table_hash['label']
		table.size = table_hash['size']
		table
	end

	def to_json dummy
		{
			:id => @id,
			:label  => @label,
			:size => @size
		}.to_json
	end

end