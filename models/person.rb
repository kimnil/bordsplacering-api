# Author Kim Nilsson, Otto Nordgren
# Stockholm 2012
class Person

	attr_accessor :id
	attr_accessor :name
	attr_accessor :gender
	attr_accessor :type
	attr_accessor :interests
	attr_accessor :placement
	attr_accessor :report

	# Same sex weight prevents male - male and female - female situations.
	@@w_same_sex = 1000 

	# Shallow type is student or company. So this prevents students - students and company - company situation.
	@@w_same_shallow_type = 2000

	# Deep type is the type of student or company. For example datateknik or tieto. So this prevents people from the 
	# same company or type of educatoin from sitting next to each other. Eg: tieto - tieto or datateknik - datateknik.
	@@w_same_deep_type = 1000

	# This method is used to measure the distance to another person to,
	# for example, set the weight in a TSP graph.
	def distance(other_person)
		one_way_match = false
		dist = 0
		debug_report = "Distance report for #{self.name} - #{other_person.name}\n"

		if self == other_person
			return 0
		end

		# Maybe we can do something smart here. If there is a place assigned already
		# make the distance very small. Maybe not? This might fuck up total cost.
		# if self.placement['seat'] != -1
		# 	return -1_000_000_000
		# end

		# Prevent same sex neighbours
		if self.gender == other_person.gender
			dist += @@w_same_sex
			debug_report << "\t+#{@@w_same_sex} because both are #{other_person.gender}s\n"
		end

		# Prevent same type (ie. student next to student, company next to company)
		['company', 'student'].any? do |key|
			same = (self.type.key?(key) && other_person.type.key?(key))

			if same == true
				dist += @@w_same_shallow_type 
				debug_report << "\t+#{@@w_same_shallow_type} because they share the shallow type (eg. company or student): #{key}\n"

				if (self.type[key] == other_person.type[key])
					dist += @@w_same_deep_type 
					debug_report << "\t+#{@@w_same_deep_type} because they share the same deep type (eg. datateknik or tieto): #{other_person.type[key]}\n"
				end
				true
			end

		end

		# If interest match, decrease distance 
		self.interests.each do |interest|
			#wanted_company = interest['type']
			#puts wanted_company.inspect
			#puts other_person.type.inspect

			if (wanted_company = interest['type']) && (wanted_company == other_person.type)
				debug_report << "\t-#{interest['weight']} #{self.name} wants #{interest['type']} and #{other_person.name} has type #{other_person.type}\n"
				dist -= interest['weight']
				other_person.interests.each do |other_persons_interest|  
					if ((wanted_type = other_persons_interest['type']) && wanted_type == self.type)	
						one_way_match = true
						dist -= other_persons_interest['weight']*2
					end
				end
			end
		end

		# Interests again but other direction
		other_person.interests.each do |other_persons_interest|
			if (wanted_company = other_persons_interest['type']) && (wanted_company == self.type)
				debug_report << "\t-#{other_persons_interest['weight']} #{other_person.name} wants #{other_persons_interest['type']} and #{self.name} has type #{self.type}\n"
				dist -= other_persons_interest['weight']
				self.interests.each do |interest|  
					if ((wanted_type = interest['type']) && wanted_type == other_person.type)	
						# if we already have on way match, we got a double match here!
						if one_way_match	
							debug_report << "\t-#{interest['weight']} & -#{other_persons_interest['weight']} two-way-match!\n"
							dist -= (interest['weight'] + other_persons_interest['weight'])
						end
					end
				end
			end
		end

		return dist, debug_report[0..-1]
	end

	def self.new_from_hash person_hash 
		unless person_hash.instance_of? Hash
			halt 500
		end

		person = Person.new
		person.id = person_hash['id']
		person.name = person_hash['name']
		person.gender = person_hash['gender']
		person.type = person_hash['type']
		person.interests = person_hash['interests']
		person.placement = person_hash['placement']
		person
	end

	def to_json dummy
		{
			:id => @id,
			:name => @name,
			:gender => @gender,
			:type => @type,
			:interests => @interests,
			:placement => @placement,
			:report => @report
		}.to_json
	end

end
